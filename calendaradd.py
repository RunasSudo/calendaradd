from settings import *

import json
import requests_oauthlib
import sys

import flask
app = flask.Flask(__name__, template_folder='jinja2')

# For authenticating clients
google = requests_oauthlib.OAuth2Session(GOOGLE_OAUTH_ID, scope=['openid', 'https://www.googleapis.com/auth/userinfo.email'], redirect_uri=GOOGLE_OAUTH_REDIR)

# For interacting with the calendar proper
with open('token.json', 'r') as f:
	token = json.load(f)
google2 = requests_oauthlib.OAuth2Session(GOOGLE_OAUTH_ID, token=token)

@app.route('/')
def index():
	return flask.render_template('index.html', CALENDAR_LABEL=CALENDAR_LABEL)

@app.route('/addme', methods=['POST'])
def addme():
	kwargs = {'prompt': 'select_account'}
	if LIMIT_DOMAIN:
		kwargs['hd'] = LIMIT_DOMAIN
	auth_url, state = google.authorization_url('https://accounts.google.com/o/oauth2/v2/auth', **kwargs)
	
	return flask.redirect(auth_url)

@app.route('/oauth')
def oauth():
	google.fetch_token('https://www.googleapis.com/oauth2/v4/token', client_secret=GOOGLE_OAUTH_SECRET, authorization_response=flask.request.url)
	response = google.get('https://www.googleapis.com/oauth2/v1/userinfo')
	
	userinfo = json.loads(response.content)
	
	if LIMIT_DOMAIN and userinfo['hd'] != LIMIT_DOMAIN and not userinfo['hd'].endswith('.' + LIMIT_DOMAIN):
		return flask.render_template('index.html', error='You must use a ' + LIMIT_DOMAIN + ' email address to use this form.', CALENDAR_LABEL=CALENDAR_LABEL)
	
	response = google2.post('https://www.googleapis.com/calendar/v3/calendars/' + GOOGLE_CAL_ID + '/acl', json={'kind': 'calendar#aclRule', 'role': 'writer', 'scope': {'type': 'user', 'value': userinfo['email']}})
	
	if response.status_code == 401:
		# Try refreshing the token
		google2.refresh_token('https://www.googleapis.com/oauth2/v4/token', client_id=GOOGLE_OAUTH_ID, client_secret=GOOGLE_OAUTH_SECRET)
		response = google2.post('https://www.googleapis.com/calendar/v3/calendars/' + GOOGLE_CAL_ID + '/acl', json={'kind': 'calendar#aclRule', 'role': 'writer', 'scope': {'type': 'user', 'value': userinfo['email']}})
	
	if response.status_code != 200:
		print('Got {} code'.format(response.status_code), file=sys.stderr)
		print(response.content, file=sys.stderr)
		return flask.render_template('index.html', error='There was an unknown error attempting to add the calendar. (Error code {})'.format(response.status_code), CALENDAR_LABEL=CALENDAR_LABEL)
	
	return flask.render_template('success.html', email=userinfo['email'], CALENDAR_LABEL=CALENDAR_LABEL)
