from settings import *

import json
import requests_oauthlib

import flask
app = flask.Flask(__name__, template_folder='jinja2')

google2 = requests_oauthlib.OAuth2Session(GOOGLE_OAUTH_ID, scope=['openid', 'https://www.googleapis.com/auth/userinfo.email', 'https://www.googleapis.com/auth/calendar'], redirect_uri=GOOGLE_OAUTH_REDIR)

@app.route('/')
def index():
	auth_url, state = google2.authorization_url('https://accounts.google.com/o/oauth2/v2/auth', access_type='offline', prompt='select_account')
	
	return flask.redirect(auth_url)

@app.route('/oauth')
def oauth():
	token = google2.fetch_token('https://www.googleapis.com/oauth2/v4/token', client_secret=GOOGLE_OAUTH_SECRET, authorization_response=flask.request.url)
	
	return json.dumps(token)
