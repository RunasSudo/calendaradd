# CalendarAdd

A simple Flask application to enable users to grant themselves write access to a Google Calendar.

## Instructions

Create `settings.py`, following the structure from `settings.example.py`. Your application will need an [OAuth 2 client ID/secret](https://console.cloud.google.com/) with access to the Google Calendar API, and corresponding `https://www.googleapis.com/auth/calendar` scope.

First run using `FLASK_APP=authorise`, e.g. `FLASK_APP=authorise python -m flask run`. Open the application in the browser, logging in with the user who owns the calendar (or who can manage the sharing settings), and copy the returned OAuth 2 token into a file called `token.json`.

Then run as usual, e.g. `FLASK_APP=calendaradd python -m flask run`.

If using HTTP, you may also need to pass the `OAUTHLIB_INSECURE_TRANSPORT=1` environment variable to force OAuth over HTTP.
